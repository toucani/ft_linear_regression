# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    fileIO.py                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/28 20:10:09 by dkovalch          #+#    #+#              #
#    Updated: 2018/04/28 20:25:11 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of ft_linear_regression project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

filename = "results.txt"


def read_weights(verbose):
    try:
        if verbose:
            print("Opening " + filename + "...")
        with open(filename) as file:
            floats = list(map(float, file.read().split()))
        return floats[0], floats[1]
    except IOError as err:
        print(err)
    except ValueError as err:
        print("Looks like " + filename + " is corrupted...")
        print(err)
    except IndexError:
        print("Looks like " + filename + " lacks the data...")
    return 0, 0


def save_weights(verbose, data):
    if verbose:
        print("Saving weights to " + filename + "...")
    try:
        with open(filename, "w") as file:
            for value in data:
                file.write("{}\n".format(value))
    except IOError as err:
        print(err)


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def read_dataset(verbose, filename):
    values = []
    try:
        if verbose:
            print("Opening " + filename + "...")
        with open(filename) as file:
            values = [line.split(",") for line in file]
        # We're expecting exactly 2 parameters
        values = filter(lambda x: len(x) == 2, values)
        values = list(map(lambda x: (str(x[0]).strip(), str(x[1]).strip()), values))
        # Removing non-numbers.
        values = filter(lambda x: is_number(x[0]) and is_number(x[1]), values)
        # Lets make it float.
        values = list(map(lambda x: (float(str(x[0])), float(str(x[1]))), values))
    except IOError as err:
        print(err)
    return list(values)
