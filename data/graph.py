# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    graph.py                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/29 10:26:18 by dkovalch          #+#    #+#              #
#    Updated: 2018/04/29 14:17:01 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of ft_linear_regression project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import matplotlib.pyplot as plt

from fileIO import read_dataset
from fileIO import read_weights
from predict import estimate


def plot_w_dataset(filename):
    # Plotting raw data
    dataset = read_dataset(False, filename)
    data_x = [x[0] for x in dataset]
    data_y = [x[1] for x in dataset]
    plt.plot(data_x, data_y, "ro")

    # Plotting prediction
    weights = read_weights(False)
    prediction_y = [estimate(weights[0], x, weights[1]) for x in data_x]
    plt.plot(data_x, prediction_y, "b")

    # Plotting prediction errrors
    err_y = [abs(data_y[x] - prediction_y[x]) for x in range(len(dataset))]
    plt.plot(data_x, err_y, "c.")

    # Plotting error mean
    data_x.sort()
    err_mean = sum(err_y) / len(err_y)
    plt.plot([data_x[0], data_x[-1]], [err_mean, err_mean], "m--")
    plt.legend(["Data", "Prediction", "Error", "Mean err"])


def plot_prediction():
    weights = read_weights(False)
    data_x = [0, 250000]
    data_y = [estimate(weights[0], x, weights[1]) for x in data_x]
    plt.plot(data_x, data_y, "b")
    plt.legend(["Prediction"])


def visualize(filename):
    filename = str(filename).strip()
    if filename:
        plot_w_dataset(filename)
    else:
        plot_prediction()

    plt.xlabel("Mileage(km)")
    plt.ylabel("Price($)")
    plt.grid(True)
    plt.show()
