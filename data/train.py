# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    train.py                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/22 21:14:20 by dkovalch          #+#    #+#              #
#    Updated: 2018/05/09 22:17:50 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of ft_linear_regression project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from fileIO import read_dataset
from fileIO import save_weights
from predict import estimate

verbose = False


def rescale(dataset):
    max_x = max([line[0] for line in dataset])
    max_y = max([line[1] for line in dataset])
    dataset = [(line[0] / max_x, line[1] / max_y) for line in dataset]
    return [max_x, max_y], dataset


def count_gradients(learningRate, dataset, slope, constant):
    sum_slope = sum_constant = 0.0
    for x, y in dataset:
        sum_slope += (estimate(slope, x, constant) - float(y)) * float(x)
        sum_constant += estimate(slope, x, constant) - float(y)
    return learningRate * sum_slope / len(dataset), \
           learningRate * sum_constant / len(dataset)


def count_weights(dataset, scales):
    entry = 0
    minDiff = 1e-7
    slope = constant = 0.0
    try:
        while True:
            entry += 1
            newSlope, newConstant = count_gradients(1e-4, dataset, slope, constant)
            if abs(newSlope) < minDiff and abs(newConstant) < minDiff:
                break

            if verbose and entry == 100000:
                entry = 0
                print("Theta 0(B):   " + str(constant * scales[1]))
                print("Theta 1(K):   " + str(slope * scales[1] / scales[0]))

            slope -= newSlope
            constant -= newConstant

    except KeyboardInterrupt:
        pass
    return slope * scales[1] / scales[0], constant * scales[1]


def train(verb, filename):
    global verbose
    verbose = verb
    dataset = read_dataset(verbose, filename)
    if len(dataset) < 1:
        print("No data!")
        return
    scales, dataset = rescale(dataset)
    save_weights(verbose, count_weights(dataset, scales))


if __name__ == "__main__":
    filename = input("Filename: ")
    train(False, filename)
