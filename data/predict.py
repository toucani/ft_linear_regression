# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    predict.py                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/22 21:14:28 by dkovalch          #+#    #+#              #
#    Updated: 2018/04/29 13:02:12 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of ft_linear_regression project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from fileIO import read_weights

verbose = False


def estimate(slope, x, constant):
    return float(slope) * float(x) + float(constant)


def get_mileage(mileage):
    try:
        mileage = float(mileage)
    except ValueError as err:
        print(err)
        mileage = -1

    while mileage < 0:
        try:
            mileage = float(input("Km: "))
            if mileage < 0:
                print("Less than zero? Really?")
        except ValueError:
            print("Oops! Try again.")
    return mileage


def predict(verb, mileage):
    global verbose
    verbose = verb
    mileage = get_mileage(mileage)
    tetas = read_weights(verbose)
    if verbose:
        print("Theta 0 (K/slope):    " + str(tetas[0]))
        print("Theta 1 (B/constant): " + str(tetas[1]))
    prediction = estimate(tetas[0], mileage, tetas[1])
    print("Price prediction: " + str(prediction))


if __name__ == "__main__":
    predict(False, -1)
