#!/usr/bin/env python3

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_regression.py                                   :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/22 18:47:41 by dkovalch          #+#    #+#              #
#    Updated: 2018/04/29 13:40:23 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of ft_linear_regression project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import getopt
import sys

from graph import visualize
from predict import predict
from train import train


def print_help():
    print("Usage:\n\tft_regression -hv [-f file] [-m mileage]")
    print("\nOptions:\n\t")
    print("\t-h, --help\t\tto print this message")
    print("\t-v, --verbose\t\tverbose output")
    print("\t-f, --file\t\tfile with dataset, to train the algorithm")
    print("\t-m, --mileage\t\tmileage to predict the price for")
    print("\t-g, --graph\t\tvisualize the data")


def main(argv):
    try:
        options, arguments = getopt.getopt(argv, "hvgf:m:",
                                           ["help", "verbose", "graph", "file=", "mileage="])

    except getopt.GetoptError as err:
        print(err)
        print("Use --help to print usage message.")
        sys.exit(1)

    if ("-h", "") in options or ("--help", "") in options:
        print_help()
        sys.exit(0)

    del arguments
    mileage = -1
    filename = ""
    graph = False
    verbose = False

    for option, value in options:
        if "-v" in option or "--verbose" in option:
            verbose = True
        elif "-g" in option or "--graph" in option:
            graph = True
        elif "-m" in option or "--mileage=" in option:
            mileage = value
        elif "-f" in option or "--file=" in option:
            filename = value

    del options
    try:
        if graph:
            visualize(filename)
        elif filename:
            train(verbose, filename)
        else:
            predict(verbose, mileage)

    except KeyboardInterrupt:
        exit(0)


if __name__ == "__main__":
    main(sys.argv[1:])