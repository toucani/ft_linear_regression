# ft_linear_regression

ft_linear_regression is the first machine learning project at UNIT Factory. A simple algorithm predicting a car price.

## Installing

Just clone this repository.
To run the programm you need python3 and pip.

## Using

```
python3 data/ft_regression.py
```

Before first usage, the algorithm should be trained:
```
python3 data/ft_regression.py -f datasets/subjectData.csv
```

|Flag|Meaning|
|---|---|
|-h, --help|print help|
|-f, --file|select the file|
|-m, --mileage|predict car price|
|-g, --graph|visualize the data
|-v, --verbose|prints out more info|

## License
![gplv3-88x31.png](https://www.gnu.org/graphics/gplv3-88x31.png)

This project is licensed under the GNU GPL License, Version 3 - see [LICENSE.md](LICENSE.md) for details.

## Contributing/Issue creating

Raise an issue in the issue tracker, or write me a letter.

## Contacts

* [Bitbucket](https://bitbucket.org/Mitriksicilian/)
* [E-mail](mailto:MitrikSicilian@icloud.com?subject=ft_linear_regression from Bitbucket)
* MitrikSicilian@icloud.com

## Many thanks

* to UNIT Factory, for inspiration to do our best.
* to all UNIT Factory students, who shared their knowledge and tested this project.

## UNIT Factory
![UNIT Factory logo](https://unit.ua/static/img/logo.png)

[UNIT Factory](https://uk.wikipedia.org/wiki/UNIT_Factory) was an innovative programming school and a part of [School 42](https://en.wikipedia.org/wiki/42_(school)) in [the heart](https://en.wikipedia.org/wiki/Kyiv) of [Ukraine](https://en.wikipedia.org/wiki/Ukraine).
